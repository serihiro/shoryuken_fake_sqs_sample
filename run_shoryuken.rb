require 'shoryuken'
require 'shoryuken/cli'

Shoryuken.configure_server do |config|
  config.aws = {
    endpoint:          'http://fake_sqs:9494',
    secret_access_key: 'secret access key',
    access_key_id:     'access key id',
    region:            'ap-northeast-1'
  }
  config.queues << 'default'
  Shoryuken.options[:concurrency] = 1
  Shoryuken.options[:delay] = 2
end

Shoryuken::CLI.instance.run(['-r', '.', '-L', './shoryuken.log'])
