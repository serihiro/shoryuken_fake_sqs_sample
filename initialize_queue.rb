require 'bundler'
Bundler.require

sqs_client = Aws::SQS::Client.new(
  endpoint:          'http://fake_sqs:9494',
  secret_access_key: 'secret access key',
  access_key_id:     'access key id',
  region:            'ap-northeast-1'
)
sqs_client.create_queue(queue_name: 'default')
