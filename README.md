# About this
This is a sample code to execute [Shoryuken](https://github.com/phstc/shoryuken) with [Fake SQS](https://github.com/iain/fake_sqs).

# Usage

```sh
$ git clone git@bitbucket.org:serihiro/shoryuken_fake_sqs_sample.git
$ cd shoryuken_fake_sqs_sample
$ docker-compose up -d
$ docker-compose exec shoryuken bash
# bundle exec ruby sample_worker.rb # enqueue job
```

# Requirements
- Docker for mac > 1.12.0
