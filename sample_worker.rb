require 'shoryuken'

Shoryuken.configure_client do |config|
  config.aws = {
    endpoint:          'http://fake_sqs:9494',
    secret_access_key: 'secret access key',
    access_key_id:     'access key id',
    region:            'ap-northeast-1'
  }
end

class SampleWorker
  include Shoryuken::Worker
  shoryuken_options queue: :default, auto_delete: true

  def perform(sqs_msg, body)
    puts 'SampleWorker start'
    puts "body: #{body}"
    puts 'SampleWorker end'
  end
end

Shoryuken::Client.sqs.send_message(queue_url: 'http://fake_sqs:9494/default', message_body: {hoge: :fuga}.to_json)
